# arm_task

Thanks for attention on todays lection.
Please take a look at this task:

*** ***
Write ARM template which will deploy:
i.	Load Balancer (Basic, Public)
ii.	Windows 2016|2019 Server vm`s (2 instances)  *** Try “Datacenter Server Core” software plan ***
iii.	Linux [your favorite available dist.]  to an Azure. 

Then template should to provide the following configuration/scenario:

-	Windows instances have common availability set 
-	Load balancer target - Windows instances
-	Linux vm should be provisioned as a Chef or Puppet server ;     *** at your convenience ***
-	All instances are members of one Virtual Network, but in different subnets:
~	Windows instances – winhosts-subnet ;
~	Linux instance – linuxserver-subnet ;
-	Public access should be allowed only for Linux instance (ability to connect over SSH) ;  *** subnet assigned NSG rule ***
-	Try to avoid managed discs usage. Configure Storage accounts as instance storage.

Recommended folder structure:

- [root_template_folder]:
-	[template_name].json 
-	[template_name].parameters.json
-	metadata.json
-	README.md
*** ***
